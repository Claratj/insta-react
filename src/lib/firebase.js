import Firebase from "firebase/compat/app";
import "firebase/compat/firestore";
import "firebase/compat/auth";

const config = {
  apiKey: "AIzaSyCIdzpTtiAVRPEEjthndz17OgWGV1YKW8w",
  authDomain: "insta-reactapp.firebaseapp.com",
  projectId: "insta-reactapp",
  storageBucket: "insta-reactapp.appspot.com",
  messagingSenderId: "542279524699",
  appId: "1:542279524699:web:4193339f0166e589917a7e",
};

const firebase = Firebase.initializeApp(config);
const { FieldValue } = Firebase.firestore;

export { firebase, FieldValue };
